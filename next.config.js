const path = require(`path`);

module.exports = {
    async redirects() {
        return [
            {
                source: '/',
                destination: '/home',
                permanent: true,
            },
        ]
    },
    future: {
        webpack5: true,
    },
    webpack: {
        alias: {
            '@modules': path.resolve(__dirname, 'src/modules'),
            '@utils': path.resolve(__dirname, 'src/utils'),
            '@redux': path.resolve(__dirname, 'src/redux'),
        },
        extensions: ['.ts', '.tsx'],
    }
}