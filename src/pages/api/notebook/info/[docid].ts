// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import db from '@modules/firebase';
import { NotebookInfo, error } from '@modules/firebase/types';

// retrieved info about a notebook


export default async (req: NextApiRequest, res: NextApiResponse<NotebookInfo | error>) => {
    const { docid } = req.query;
    console.log('req:', docid);

    if (typeof docid === 'string') {
        try {
            const docRef = await db.collection('id_map').doc(docid).get();

            if (docRef.exists) {
                const docData = docRef.data() as NotebookInfo;
                res.status(200).json(docData);
            } else {
                res.status(400).send({ info: 'Unable to find document!' });
            }

        } catch (e) {
            console.log('Error retrieving firebase notebook info', { e });
            res.status(400).send({
                info: 'Error retrieving firebase notebook info!',
                moreInfo: JSON.stringify(e)
            });
        }
    } else {
        // It can either be a string[] or a string
        // we know it will never be a string[], because deeper nested apis won't work
        // Thus, this shouldn't happen, but is primarily here for typechecking
        res.status(400).end();
    }

}