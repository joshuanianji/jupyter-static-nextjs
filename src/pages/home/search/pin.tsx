import { Text, Grid, Input, Spacer, Button, useToasts } from '@geist-ui/react';
import { ArrowRight } from '@geist-ui/react-icons';
import React, { useState } from 'react';
import { sleep } from '@utils/index';
import {
    selectNotebook,
    useAppDispatch,
    useAppSelector,
    updatePinID,
    inputPin,
    addNotebook
} from '@redux/exports';

// Input pin for jupyter notebook

const SearchPin: React.FC = () => {
    // local state
    const [, setToast] = useToasts();
    const [loading, setLoading] = useState(false);

    // redux
    const { inputState, id, pin } = useAppSelector(selectNotebook);
    const dispatch = useAppDispatch();

    const handleKeyDown = async (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            findNotebookInfo()
        }
    }

    const findNotebookInfo = async () => {
        setLoading(true);
        await sleep(1000);
        setLoading(false);
    }

    if (inputState === 'id') {
        return null;
    } else {
        // display the other thing
        return (
            <Grid xs={24} justify='center'>
                <Input size='large'
                    onChange={(e) => dispatch(updatePinID(e.target.value))}
                    onKeyDown={handleKeyDown}
                    value={pin} />
                <Spacer x={1} />
                <Button
                    icon={<ArrowRight />} auto
                    disabled={pin === '' || loading}
                    loading={loading}
                    onClick={() => findNotebookInfo()}></Button>
            </Grid>
        )
    }
}

export default SearchPin;