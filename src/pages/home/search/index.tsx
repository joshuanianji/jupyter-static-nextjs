import { Grid, Text } from '@geist-ui/react';
import React from 'react';
import SearchID from './id';
import SearchPin from './pin';
import {
    selectNotebook,
    useAppDispatch,
    useAppSelector,
    updatePinID,
    inputPin,
    addNotebook
} from '@redux/exports';

const NotebookSearch: React.FC = () => {
    const { inputState } = useAppSelector(selectNotebook);

    return (
        <Grid.Container gap={3}>
            <Grid xs={24} justify='center'>
                <Text h3>{inputState === 'id' ? 'Search ID' : 'Input Pin'}</Text>
            </Grid>
            <SearchID />
            <SearchPin />
        </Grid.Container>
    )
}

export default NotebookSearch;