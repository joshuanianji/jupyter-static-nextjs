import { Text, Grid, Input, Spacer, Button, useToasts } from '@geist-ui/react';
import { X, ArrowRight } from '@geist-ui/react-icons';
import React, { useState } from 'react';
import axios from 'axios';
import { NotebookInfo } from '@modules/firebase/types';
import {
    selectNotebook,
    useAppDispatch,
    useAppSelector,
    updateSearchID,
    inputPin,
    addNotebook,
    exitPinInput,
} from '@redux/exports';

// Search for the jupyter ID

const SearchID: React.FC = () => {
    // local state
    const [, setToast] = useToasts();
    const [loading, setLoading] = useState(false);

    // redux
    const { inputState, id, pin } = useAppSelector(selectNotebook);
    const dispatch = useAppDispatch();

    const handleKeyDown = async (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            await findNotebookInfo();
        }
    }

    const findNotebookInfo = async () => {
        setLoading(true);
        try {
            const data = await axios.get<NotebookInfo>(`/api/notebook/info/${id}`);
            if (data.data.pinRequired) {
                // change to input
                setToast({ text: 'Found notebook w/ pin requirement', type: 'success' });
                console.log('changing to pin input...');
                dispatch(inputPin());
            } else {
                setToast({ text: 'Found notebook w/o pin requirement', type: 'success' });
                console.log('no pins poggies');
                dispatch(addNotebook(data.data));
            }
        } catch (error) {
            console.log('error!', { error });
            // usually 400 
            if (error.response.status === 400) {
                setToast({ text: 'Could not retrieve notebook!', type: 'error' });
            } else {
                setToast({ text: 'Unknown error retrieving notebook!', type: 'error' });
            }
        }
        // reset state and ID value
        dispatch(updateSearchID(''));
        setLoading(false);
    }

    if (inputState === 'id') {
        return (
            <Grid xs={24} justify='center'>
                {/* Not sure if `value={idValue}` is smart here.  */}
                <Input size='large'
                    onChange={(e) => dispatch(updateSearchID(e.target.value))}
                    onKeyDown={handleKeyDown}
                    value={id} />
                <Spacer x={1} />
                <Button
                    icon={<ArrowRight />} auto
                    disabled={id === '' || loading}
                    loading={loading}
                    onClick={() => findNotebookInfo()}></Button>
            </Grid>
        )
    } else {
        // display the other thing
        return (
            <Grid xs={24} justify='center'>
                <Button
                    icon={<X />} auto
                    onClick={() => dispatch(exitPinInput())}>
                    Exit
                </Button>
            </Grid>

        )
    }
}

export default SearchID;