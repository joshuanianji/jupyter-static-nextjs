import Head from 'next/head';
import { Page, Text, Grid, Input, Spacer, Button, useToasts } from '@geist-ui/react';
import { ArrowRight } from '@geist-ui/react-icons';
import React, { useState } from 'react';
import { sleep } from '@utils/index';
import NotebookSearch from './search';

const Home: React.FC = (props) => {
    return (
        <div>
            <Head>
                <title>My page title</title>
            </Head>
            <Page dotBackdrop size="mini">
                <Header />
                <Page.Content>
                    <Spacer x={8} />
                    <NotebookSearch />
                </Page.Content>
                <Footer />
            </Page>
        </div>
    )
}

const Header: React.FC = () => (
    <Page.Header>
        <Text h1 >Jupyter Static</Text>
        <Text type='secondary' small>Created by the AHS Community Health Team</Text>
    </Page.Header>
)

const Footer: React.FC = () => (
    <Page.Footer >
        <Grid.Container gap={2} justify="center" style={{ marginBottom: '8px' }}>
            <Text type='secondary' small style={{ textAlign: 'center' }}>Footers are cool</Text>
        </Grid.Container>
    </Page.Footer>
)

export default Home;