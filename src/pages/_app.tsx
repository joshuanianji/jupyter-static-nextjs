import { GeistProvider, CssBaseline } from '@geist-ui/react';
import type { AppProps } from 'next/app';
import store from '@redux/store';
import { Provider } from 'react-redux';


const App = ({ Component, pageProps }: AppProps) => (
    <Provider store={store}>
        <GeistProvider>
            <CssBaseline />
            <Component {...pageProps} />
        </GeistProvider>
    </Provider>
)

export default App;
