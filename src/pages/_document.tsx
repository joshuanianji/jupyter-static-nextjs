import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document';
import { CssBaseline } from '@geist-ui/react';

// stolen from https://github.com/geist-org/react/blob/master/examples/create-next-app/pages/_document.js
// this feels really janky though :/

const MyDocument = (props: any) => {
    return (
        <Html>
            <Head />
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}

MyDocument.renderDocument = Document.renderDocument;
MyDocument.getInitialProps = async (ctx: DocumentContext) => {
    const initialProps = await Document.getInitialProps(ctx);
    const styles = CssBaseline.flush()

    return {
        ...initialProps,
        styles: (
            <>
                {initialProps.styles}
                {styles}
            </>
        ),
    }
}

export default MyDocument;
