import * as admin from 'firebase-admin';

const credentials = {
    projectId: process.env.FIREBASE_PROJECT_ID,
    privateKey: process.env.FIREBASE_ADMIN_KEY,
    clientEmail: process.env.FIREBASE_ADMIN_CLIENT_EMAIL,
}

if (!admin.apps.length) {
    try {
        admin.initializeApp({
            credential: admin.credential.cert(credentials),
            databaseURL: 'https://ahs-playground.firebaseio.com',
        });
    } catch (error) {
        console.log('Firebase admin initialization error', error.stack);
    }

}

export default admin.firestore();
