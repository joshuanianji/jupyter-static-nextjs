interface NotebookInfo {
    location: string,
    pinRequired: boolean,
    name: string,
}

interface error {
    info: string,
    moreInfo?: string,
}

export type { error, NotebookInfo }