/** 
 * Pause execution in an asynchronous funciton (mainly for debugging)
 * @param {number} ms millieconds to delay
 */
const sleep = (ms: number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export { sleep };