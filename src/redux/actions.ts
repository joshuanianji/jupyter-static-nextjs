import * as Action from './actionTypes';
import { NotebookInfo } from '@modules/firebase/types';


const updateSearchID = (id: string): Action.UpdateSearchAction => ({
    type: Action.UPDATE_SEARCH_ID,
    payload: {
        id: id
    }
});

const updatePinID = (pin: string): Action.UpdatePinAction => ({
    type: Action.UPDATE_PIN,
    payload: {
        pin: pin
    }
});

const inputPin = (): Action.InputPinAction => ({
    type: Action.INPUT_PIN
});

const exitPinInput = (): Action.ExitPinAction => ({
    type: Action.EXIT_PIN
});

const addNotebook = (notebook: NotebookInfo): Action.AddNotebookAction => ({
    type: Action.ADD_AUTHORIZED_NOTEBOOK,
    payload: {
        notebook: notebook
    }
})

export { updateSearchID, updatePinID, inputPin, addNotebook, exitPinInput };