import * as Action from '../actionTypes';
import { AuthorizedNotebook, NotebookState } from './types';
import { Action as ActionType } from './../actionTypes';

const initialState: NotebookState = {
    inputState: 'id',
    id: '',
    pin: '',
    authorizedNotebooks: []
}

export default function reducer(state = initialState, action: ActionType): NotebookState {
    switch (action.type) {
        case Action.UPDATE_SEARCH_ID: {
            const { id } = action.payload;
            return {
                ...state,
                id: id
            };
        }
        case Action.UPDATE_PIN: {
            const { pin } = action.payload;
            return {
                ...state,
                pin: pin,
            };
        }
        case Action.INPUT_PIN: {
            return {
                ...state,
                inputState: 'pin'
            };
        }
        case Action.EXIT_PIN: {
            return {
                ...state,
                inputState: 'id'
            };
        }
        case Action.ADD_AUTHORIZED_NOTEBOOK: {
            const newNotebook: AuthorizedNotebook = {
                protected: action.payload.notebook.pinRequired,
                name: action.payload.notebook.name,
                location: action.payload.notebook.location,
            }
            return {
                ...state,
                authorizedNotebooks: [...state.authorizedNotebooks, newNotebook]
            };
        }
        default:
            return state;
    }
}
