// these are NOT exposed to the client

type inputState = 'id' | 'pin';

export interface NotebookState {
    inputState: inputState,
    id: string,
    pin: string,
    authorizedNotebooks: AuthorizedNotebook[]
}

export interface AuthorizedNotebook {
    protected: boolean, // if they are pwd protected or not
    name: string,
    location: string,
}