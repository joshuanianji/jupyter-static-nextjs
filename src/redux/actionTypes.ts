import { Action as ReduxAction } from 'redux';
import { NotebookInfo } from '@modules/firebase/types';


export const UPDATE_SEARCH_ID = 'UPDATE_SEARCH_ID';
export const UPDATE_PIN = 'UPDATE_PIN';
export const INPUT_PIN = 'INPUT_PIN';
export const EXIT_PIN = 'EXIT_PIN'; // Cancel the pin thing and go back to searching for IDs
export const ADD_AUTHORIZED_NOTEBOOK = 'ADD_AUTHORIZED_NOTEBOOK';

export type ActionType = 'UPDATE_SEARCH_ID' | 'UPDATE_PIN' | 'INPUT_PIN' | 'ADD_AUTHORIZED_NOTEBOOK' | 'EXIT_PIN';


export interface UpdateSearchAction extends ReduxAction<ActionType> {
    type: 'UPDATE_SEARCH_ID',
    payload: {
        id: string
    }
}

export interface UpdatePinAction extends ReduxAction<ActionType> {
    type: 'UPDATE_PIN',
    payload: {
        pin: string
    }
}

export interface InputPinAction extends ReduxAction<ActionType> {
    type: 'INPUT_PIN'
}

export interface ExitPinAction extends ReduxAction<ActionType> {
    type: 'EXIT_PIN'
}

export interface AddNotebookAction extends ReduxAction<ActionType> {
    type: 'ADD_AUTHORIZED_NOTEBOOK',
    payload: {
        notebook: NotebookInfo
    }
}

export type Action = UpdateSearchAction | UpdatePinAction | InputPinAction | AddNotebookAction | ExitPinAction;