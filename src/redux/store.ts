import { createStore } from 'redux';
import rootReducer from './reducers';


const store = createStore(rootReducer);
export default store;

// https://redux.js.org/recipes/usage-with-typescript
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
