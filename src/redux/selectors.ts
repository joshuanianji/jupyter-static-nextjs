import { RootState } from './store'

// since the notebook is kind of at the "root" store level
// getNotebook is just the identity function
export const getNotebook = (store: RootState) => store;
